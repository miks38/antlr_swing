tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;

import org.antlr.stringtemplate.StringTemplateGroup;
}



@members {
  Integer numer = 0;
}
prog    : (e+=expr_or_code_block | fdl+=func_decl)* -> prog(block={$e}, varDecl={getDeclarations()}, funDecl={$fdl});


decl  :
        ^(VAR i1=ID) {newVarId($i1.text);} ->
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}


func_decl : 
            ^(FUNCTION i1=ID code_block) {newFuncId($ID.text);} -> funkcja(n={$ID.text}, blok={$code_block.st}) 
          ;
          catch [RuntimeException ex] {errorID(ex, $i1);}


expr_or_code_block :
      e=expr -> {$e.st}
      | cd=code_block -> {$cd.st}
      | d=decl -> 
      | ifb=if_block -> {$ifb.st} 
      ; 


if_block :
    {afterIf();}
    ^(IF_ELSE_ROOT ^(COND boolExpr) ^(IF ifblock=code_block) (^(ELSE elseblock=code_block))? ) 
    -> ifTemplate(c={$boolExpr.st}, ifblock={$ifblock.st}, elseblock={$elseblock.st}, elselabel={elseLabel()}, endiflabel={endIfLabel()}, jump={$boolExpr.jump})  
    ;
      


code_block : 
			        {enterScope();}
			        ^(LB  (v+=expr_or_code_block)*)
			        {leaveScope();} -> blok_kodu(blok={$v})
           ;
             

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> minus(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=id_expr   e2=expr) -> podstaw(p1={$i1.st},p2={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | id_expr                  -> template(i1={$id_expr.st}) "MOV A,<i1>" 
        | ^(RETURN e1=expr)        -> zwroc(p1={$e1.st})
        | ^(FUNCTION_CALL ID)      -> wywolaj(n={annotatedFuncId($ID.text)}) 
    ;


boolExpr returns [String jump] :
       ^(cmpOperator e1=expr e2=expr) {$jump = $cmpOperator.jump;} -> compare(p1={$e1.st},p2={$e2.st})
    ;


cmpOperator returns [String jump] : EQ {$jump="JNE";}
                                  |  NE {$jump="JE";}
                                  |  GR {$jump="JLE";}
                                  |  GQ {$jump="JL";}
                                  |  LE {$jump="JGE";}
                                  |  LQ {$jump="JG";}
                                  ;

id_expr: i1=ID -> id(i={annotatedId($ID.text)});
          catch [RuntimeException ex] {errorID(ex, $i1);}
    