/**
 * 
 */
package tb.antlr.kompilator;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.HashSet;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.TreeAdaptor;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;
import org.antlr.runtime.debug.DebugEventListener;
import org.antlr.runtime.debug.DebugEventSocketProxy;
import org.antlr.runtime.debug.DebugTokenStream;
import org.antlr.runtime.debug.DebugTreeAdaptor;
import org.antlr.runtime.debug.DebugTreeParser;
import org.antlr.stringtemplate.*;

import tb.antlr.symbolTable.GlobalSymbols;



public class TreeParserTmpl extends TreeParser /*DebugTreeParser*/ {

	/*
	public TreeParserTmpl(TreeNodeStream input, DebugEventListener dbg_, RecognizerSharedState state) {
		super(input, dbg_, state);
	}
	*/
	
	
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) throws RuntimeException {
		super(input, state);
		try {
			scopes = new Scopes();
		} catch (IOException e) {
			System.out.println("Catched: " + e.getMessage());
			throw new RuntimeException("File something");
		}
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	private class Scopes {
		private class Scope {
			private Scopes parent;
			private int scope_id;
			private HashSet<String> var_names;
			
			public Scope(int _scope_id, Scopes _parent) {
				parent = _parent;
				scope_id = _scope_id;
				var_names = new HashSet<String>();
			}
			
			public String annotate(String var_name) {
				return var_name + "_" + Integer.toString(scope_id);
			}
			
			public boolean contains(String var_name) {
				return var_names.contains(var_name);
			}
			
			public String newVarName(String var_name) throws RuntimeException {
				if (var_names.contains(var_name)) {
					throw new RuntimeException("Scopes::newVarName: \"" + var_name + "\" already exists");
				}
				
				var_names.add(var_name);
				String annotated = annotate(var_name);
				parent.addDeclaration(annotated);
				return annotated;
			}
		}
		
		private ArrayList<StringTemplate> declarations;
		private StringTemplateGroup pierwszy;
		private Stack<Scope> scopes;
		private int next_scope;
		
		private void addDeclaration(String annotatedVarName) {
			StringTemplate t = pierwszy.getInstanceOf("dek");
			t.setAttribute("n", annotatedVarName);
			declarations.add(t);
		}
		
		public Scopes() throws IOException {
			declarations = new ArrayList<StringTemplate>();
			
			InputStream is = getClass().getResourceAsStream("pierwszy.stg");
			Reader pierwszyReader = new InputStreamReader(is);
			pierwszy = new StringTemplateGroup(pierwszyReader);
			pierwszyReader.close();
			
			scopes = new Stack<Scope>();
			next_scope = 0;
			enterScope();
		}
		
		public void enterScope() {
			scopes.add(new Scope(next_scope, this));
			next_scope += 1;
		}
		
		public void leaveScope() {
			scopes.pop();
		}
		
		public String decodeVarName(String var_name) throws RuntimeException {
			Scope var_scope = null;
			
			for (int i = scopes.size() - 1; i >= 0; i--) {
				if (scopes.get(i).contains(var_name)) {
					var_scope = scopes.get(i);
					break;
				}
			}
			
			if (var_scope == null) {
				throw new RuntimeException("Var name: \"" + var_name + "\" not found in any of the scopes");
			}
			
			return var_scope.annotate(var_name);
		}
		
		public String newVar(String var_name) {
			return scopes.peek().newVarName(var_name);
		}
		
		public ArrayList<StringTemplate> getDeclarations() {
			return declarations;
		}
	}
	
	private Scopes scopes;
	
	protected void enterScope() {
		scopes.enterScope();
	}
	
	protected void leaveScope() {
		scopes.leaveScope();
	}
	
	protected String annotatedId(String id) {
		return scopes.decodeVarName(id);
	}
	
	protected String newVarId(String id) {
		return scopes.newVar(id);
	}
	
	protected ArrayList<StringTemplate> getDeclarations() {
		return scopes.getDeclarations();
	}
	
	
	/**
	 * if related stuff 
	 */
	
	private int if_idx = 0;
	
	protected String elseLabel()
	{
		return "else_label_" + Integer.toString(if_idx);
	}
	
	protected String endIfLabel()
	{
		return "endif_label_" + Integer.toString(if_idx);
	}
	
	protected void afterIf()
	{
		if_idx += 1;
	}
	
	/**
	 * function related stuff
	 */
	private HashSet<String> functionIds = new HashSet<String>();
	
	protected String newFuncId(String id) throws RuntimeException {
		if (functionIds.contains(id)) {
			throw new RuntimeException("newFuncId: \"" + id + "\" already in use");
		}
		
		functionIds.add(id);
		return id;
	}
	
	protected String annotatedFuncId(String id) throws RuntimeException {
		if (!functionIds.contains(id)) {
			throw new RuntimeException("func: \"" + id + "\" was not declared");
		}
		
		return id;
	}
}
