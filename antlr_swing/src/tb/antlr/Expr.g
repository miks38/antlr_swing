grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;


}

@lexer::header {
package tb.antlr;
}



prog
    : (stat | group | if_else_block | func_decl)+ EOF!;

group
    : LB^ (stat | group | if_else_block)* RB!  
    ; 

func_group
    : LB^ (stat | group | if_else_block | return_expr)* RB!
    ;

stat
    : expr EOE -> expr
    | VAR ID PODST expr EOE -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID EOE -> ^(VAR ID)
    | ID PODST expr EOE -> ^(PODST ID expr)
    | EOE ->
    ;
    
func_decl : FUNCTION ID LP RP func_group -> ^(FUNCTION ID func_group); 

func_call : ID LP RP -> ^(FUNCTION_CALL ID);

return_expr : RETURN expr EOE -> ^(RETURN expr) ;
    
if_else_block
    : IF LP cond_block RP if_block (
                        ->  ^(IF_ELSE_ROOT cond_block if_block)
      | ELSE else_block ->  ^(IF_ELSE_ROOT cond_block if_block else_block)
    )
    ;

cond_block : boolExpr -> ^(COND boolExpr);
if_block : group -> ^(IF group);
else_block : group -> ^(ELSE group);

boolExpr
    : expr cmpOperator expr -> ^(cmpOperator expr expr)
    ;

cmpOperator : EQ | NE | GR | GQ | LE | LQ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
    : atom
      (POW^ atom)*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    | func_call
    ;


IF_ELSE_ROOT: 'if_else_root';
COND: 'cond';
IF  : 'if';
ELSE: 'else';
VAR :'var';
FUNCTION : 'function';
FUNCTION_CALL : 'function_call';
RETURN : 'return';
ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;
INT : '0'..'9'+;
EOE : ';';
WS : (' ' | '\t' | '\r' | '\n')+ {$channel = HIDDEN;} ;
LP :	'(';
RP :	')';
LB : '{';
RB : '}';
PODST :	'=';
PLUS : '+';
MINUS : '-';
MUL : '*';
DIV : '/';
POW : '**'; 
EQ  : '==';
NE  : '!=';
GR  : '>';
GQ  : '>=';
LE  : '<';
LQ  : '<=';