package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.debug.*;
import org.antlr.runtime.debug.BlankDebugEventListener;
import org.antlr.runtime.debug.RemoteDebugEventSocketListener;
//import org.antlr.runtime.debug.;
import org.antlr.runtime.tree.TreeNodeStream;

public class DebugMyTreeParser extends DebugTreeParser {
    public DebugMyTreeParser(TreeNodeStream input, DebugEventListener dbg, RecognizerSharedState state) {
        super(input, dbg, state);
    }
    
    public DebugMyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
    
    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' ').replace(';', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
