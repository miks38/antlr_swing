tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
  superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;

import tb.antlr.symbolTable.*;
}

prog    
      scope { LocalSymbols syms; }
      @init { $prog::syms = new LocalSymbols(); }
          : (
           e=expr  { drukuj($e.out.toString()); }
           | sv=newVar { drukuj("new var: " + $sv.out); }
           | group
        )*;
          
          
group
        : ^(LB { $prog::syms.enterScope(); }
        (
        e=expr  {drukuj($e.text + " = " + $e.out.toString());}
        | sv=newVar {drukuj("new var: " + $sv.out);}
        | group
        { $prog::syms.leaveScope(); } 
        )*);


expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(PODST i1=ID   e2=expr) {$out = $e2.out; $prog::syms.setSymbol($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = $prog::syms.getSymbol($ID.text);}
        ;


newVar returns [String out]
        :  ^(VAR ID) {$out = $ID.text; $prog::syms.newSymbol($ID.text);}
        ;